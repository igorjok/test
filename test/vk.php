<?

//last change
$max_calls_to_api = 5;
$link= $_POST['url'] ;
$handler = false; 
$args = false;
$max_winners = $_POST['win'] ;
$konkurs_type = $_POST['vid'] ;

if ($konkurs_type == 'Like') {
    $handler = vk_like_winner;
    $args = process_wall_url($link);
}
if ($konkurs_type == 'Repost') {
    $handler = vk_repost_winner;
    $args = process_wall_url($link);
}
if ($konkurs_type == 'Group') {
    $handler = vk_group_winner;
    $args = process_group_url($link);
}
if ($konkurs_type == 'LikeGroup') {
    $handler = vk_like_winner_ingroup;
    $args = process_wall_url($link);
}
if ($konkurs_type == 'RepostGroup') {
    $handler = vk_repost_winner_ingroup;
    $args = process_wall_url($link);
}

if($handler && is_callable($handler) && !empty($args)) {
    call_user_func_array($handler, $args);
}

function process_wall_url ($link) {
    $matches = array();
    preg_match('/w=wall-(\d+)_(\d+)/', $link, $matches);
    if(empty($matches)) {
        exit('<br>Вы вставили некорректную ссылку') ;
    } 
    return array($matches[1],$matches[2]);
}

function process_group_url ($link) {
        $matches = array();
        $pars = parse_url ($link);
        $host = $pars[host];
    if ($host!='m.vk.com' and $host!='vk.com') {
        exit('<br>Вы вставили некорректную ссылку') ;  
    }
        preg_match('/club(\d+)/', $link, $matches);
    if (!empty($matches)){
        return array($matches[1]);
    }
    if(empty($matches)) {
        $pars = parse_url ($link);
        $target = substr($pars[path], 1);
        $matches[] = $target;
            if(empty($matches)) {
        exit('<br>Вы вставили некорректную ссылку') ;  
            }
        return array($matches[0]);
    } 
}

function random_array ($max_winners, $count) {
        $limit = $max_winners; //количество победителей
        $max_num = $count; //количество участников
    if($max_num == 0){
        echo '<br> У вас слишком скучный конкурс, в нем никто не участвует :(';
    }
    if($max_num<$limit) { 
        $limit = $max_num; 
    }
        $used_nums = array(); 
    while(count($used_nums) < $limit) { 
        $random = rand(0, ($max_num-1));
        if(!in_array($random, $used_nums)) { 
                $used_nums[] = $random; 
        } 
    } 
        return $used_nums;
}

function vk_like_winner ($group_id, $post_id) {
        global $max_calls_to_api, $max_winners; 
            $api_count = file_get_contents('https://api.vk.com/method/likes.getList?type=post&owner_id=-'.$group_id.'&item_id='.$post_id.'&filter=likes&friends_only=0&extended=0&offset=0&count=1&skip_own=0&access_token=813ac24f813ac24f813ac24f59815fe9228813a813ac24fda71adf311b9b9de08a5ee6f&v=5.80');
            $request = json_decode($api_count, true);           /*Запрос на количество лайков к указанному посту*/
            $count = $request['response']['count'];
            $used_nums =  random_array ($max_winners, $count);
        foreach ($used_nums as $winners) {
            $api_winner_id = file_get_contents('https://api.vk.com/method/likes.getList?type=post&owner_id=-'.$group_id.'&item_id='.$post_id.'&filter=likes&friends_only=0&extended=0&offset='.$winners.'&count=1&skip_own=0&access_token=813ac24f813ac24f813ac24f59815fe9228813a813ac24fda71adf311b9b9de08a5ee6f&v=5.80');
            $request2 = json_decode($api_winner_id, true);      /*Запрос на id страницы выбранного лайка*/
            $winner_id = $request2['response']['items'][0];      /*Определяем id победителя*/
            $api_winner = file_get_contents ('https://api.vk.com/method/users.get?user_ids='.$winner_id.'&fields=photo_100&name_case=Nom&lang=0&access_token=813ac24f813ac24f813ac24f59815fe9228813a813ac24fda71adf311b9b9de08a5ee6f&v=5.80');
            $request3 = json_decode($api_winner, true);         /*Запрос данных победятеля по id, Имя, Фамилия, Фото*/
        if ($request3['response'] == []) {                          /*Если предыдущий запрос пришел пустой, делаем запрос заново*/
                $max_calls_to_api--;
                if($max_calls_to_api < 0){
                    echo "С нашим сайтом что то пошло не так :( Попробуйте ещё раз, чуть позже";
                    return false;
                }
                vk_like_winner ($group_id, $post_id);
        }
        else {
            $winner_name = $request3 ['response'][0]['first_name'];
            $winner_last_name = $request3 ['response'][0]['last_name'];
            $winner_photo = $request3 ['response'][0]['photo_100'];
            echo 'Победитель:<br>' . '<a href="https://vk.com/id'.$winner_id.'" target="_blank";>' .'<img src='.$winner_photo.' alt='.$winner_name.'>' . $winner_name ." ". $winner_last_name . '</a>' . '<br>';
        }
    } 
}

function vk_repost_winner ($group_id, $post_id) {
        global $max_calls_to_api, $max_winners; 
            $api_count = file_get_contents('https://api.vk.com/method/likes.getList?type=post&owner_id=-'.$group_id.'&item_id='.$post_id.'&filter=copies&friends_only=0&extended=0&offset=0&count=1&skip_own=0&access_token=813ac24f813ac24f813ac24f59815fe9228813a813ac24fda71adf311b9b9de08a5ee6f&v=5.80');
            $request = json_decode($api_count, true);           /*Запрос на количество репостов к указанному посту*/
            $count = $request['response']['count'];
            $used_nums =  random_array ($max_winners, $count);
        foreach ($used_nums as $winners) {
            $api_winner_id = file_get_contents('https://api.vk.com/method/likes.getList?type=post&owner_id=-'.$group_id.'&item_id='.$post_id.'&filter=copies&friends_only=0&extended=0&offset='.$winners.'&count=1&skip_own=0&access_token=813ac24f813ac24f813ac24f59815fe9228813a813ac24fda71adf311b9b9de08a5ee6f&v=5.80');
            $request2 = json_decode($api_winner_id, true);      /*Запрос на id страницы выбранного репоста*/
            $winner_id = $request2['response']['items'][0];
            $api_winner = file_get_contents ('https://api.vk.com/method/users.get?user_ids='.$winner_id.'&fields=photo_100&name_case=Nom&lang=0&access_token=813ac24f813ac24f813ac24f59815fe9228813a813ac24fda71adf311b9b9de08a5ee6f&v=5.80');
            $request3 = json_decode($api_winner, true);         /*Запрос данных победятеля по id, Имя, Фамилия, Фото*/
        if ($request3['response'] == []) {                          /*Если предыдущий запрос пришел пустой, делаем запрос заново*/
                $max_calls_to_api--;
                if($max_calls_to_api < 0){
                    echo "С нашим сайтом что то пошло не так :( Попробуйте ещё раз, чуть позже";
                    return false;
                }
                vk_repost_winner ($group_id, $post_id);
        }
        else {
            $winner_name = $request3 ['response'][0]['first_name'];
            $winner_last_name = $request3 ['response'][0]['last_name'];
            $winner_photo = $request3 ['response'][0]['photo_100'];
            echo 'Победитель:<br>' . '<a href="https://vk.com/id'.$winner_id.'" target="_blank";>' .'<img src='.$winner_photo.' alt='.$winner_name.'>' . $winner_name ." ". $winner_last_name . '</a>' . '<br>';
        }
    }
}

function vk_group_winner ($group_id) {
        global $max_calls_to_api, $max_winners; 
            $api_count = file_get_contents('https://api.vk.com/method/groups.getMembers?group_id='.$group_id.'&offset=0&count=1&access_token=813ac24f813ac24f813ac24f59815fe9228813a813ac24fda71adf311b9b9de08a5ee6f&v=5.80');
            $request = json_decode($api_count, true);           /*Запрос на количество участников группы*/
            $count = $request['response']['count'];     
            $used_nums =  random_array ($max_winners, $count);
        foreach ($used_nums as $winners) {    
            $api_winner_id = file_get_contents('https://api.vk.com/method/groups.getMembers?group_id='.$group_id.'&offset='.$winners.'&count=1&access_token=813ac24f813ac24f813ac24f59815fe9228813a813ac24fda71adf311b9b9de08a5ee6f&v=5.80');
            $request2 = json_decode($api_winner_id, true);      /*Запрос id выбранного участника*/
            $winner_id = $request2['response']['items'][0];        /*Определяем id победителя*/
            $api_winner = file_get_contents ('https://api.vk.com/method/users.get?user_ids='.$winner_id.'&fields=photo_100&name_case=Nom&lang=0&access_token=813ac24f813ac24f813ac24f59815fe9228813a813ac24fda71adf311b9b9de08a5ee6f&v=5.80');
            $request3 = json_decode($api_winner, true);         /*Запрос данных победятеля по id, Имя, Фамилия, Фото*/
        if ($request3['response'] == []) {                          /*Если предыдущий запрос пришел пустой, делаем запрос заново*/
                $max_calls_to_api--;
                if($max_calls_to_api < 0){
                    echo "С нашим сайтом что то пошло не так :( Попробуйте ещё раз, чуть позже";
                    return false;
                }
                vk_group_winner ($group_id);
        }
        else {
            $winner_name = $request3 ['response'][0]['first_name'];
            $winner_last_name = $request3 ['response'][0]['last_name'];
            $winner_photo = $request3 ['response'][0]['photo_100'];
            echo 'Победитель:<br>' . '<a href="https://vk.com/id'.$winner_id.'" target="_blank";>' .'<img src='.$winner_photo.' alt='.$winner_name.'>' . $winner_name ." ". $winner_last_name . '</a>' . '<br>';
        }
    }
}

function vk_like_winner_ingroup ($group_id, $post_id) {
        global $max_calls_to_api, $max_winners; 
            $api_count = file_get_contents('https://api.vk.com/method/likes.getList?type=post&owner_id=-'.$group_id.'&item_id='.$post_id.'&filter=likes&friends_only=0&extended=0&offset=0&count=1&skip_own=0&access_token=813ac24f813ac24f813ac24f59815fe9228813a813ac24fda71adf311b9b9de08a5ee6f&v=5.80');
            $request = json_decode($api_count, true);           /*Запрос на количество лайков к указанному посту*/
            $count = $request['response']['count'];     
            $used_nums =  random_array ($max_winners, $count);
        foreach ($used_nums as $winners) {     
            $api_winner_id = file_get_contents('https://api.vk.com/method/likes.getList?type=post&owner_id=-'.$group_id.'&item_id='.$post_id.'&filter=likes&friends_only=0&extended=0&offset='.$winners.'&count=1&skip_own=0&access_token=813ac24f813ac24f813ac24f59815fe9228813a813ac24fda71adf311b9b9de08a5ee6f&v=5.80');
            $request2 = json_decode($api_winner_id, true);      /*Запрос на id страницы выбранного лайка*/
            $winner_id = $request2['response']['items'][0];      /*Определяем id победителя*/
            $api_group_member = file_get_contents('https://api.vk.com/method/groups.isMember?group_id='.$group_id.'&user_id='.$winner_id.'&extended=1&access_token=813ac24f813ac24f813ac24f59815fe9228813a813ac24fda71adf311b9b9de08a5ee6f&v=5.80');
            $request3 = json_decode($api_group_member, true);      /*Запрос на то состоит ли в группе человек*/
            $ismember = $request3['response']['member'];
        if ($ismember == 1) {
            $api_winner = file_get_contents ('https://api.vk.com/method/users.get?user_ids='.$winner_id.'&fields=photo_100&name_case=Nom&lang=0&access_token=813ac24f813ac24f813ac24f59815fe9228813a813ac24fda71adf311b9b9de08a5ee6f&v=5.80');
            $request4 = json_decode($api_winner, true);         /*Запрос данных победятеля по id, Имя, Фамилия, Фото*/
                    if ($request4['response'] == []) {                          /*Если предыдущий запрос пришел пустой, делаем запрос заново*/
                            $max_calls_to_api--;
                            if($max_calls_to_api < 0){
                                echo "С нашим сайтом что то пошло не так :( Попробуйте ещё раз, чуть позже";
                                return false;
                            }
                            vk_like_winner_ingroup ($group_id, $post_id);
                    }
                    else {
                        $winner_name = $request4 ['response'][0]['first_name'];
                        $winner_last_name = $request4 ['response'][0]['last_name'];
                        $winner_photo = $request4 ['response'][0]['photo_100'];
                        echo 'Победитель:<br>' . '<a href="https://vk.com/id'.$winner_id.'" target="_blank";>' .'<img src='.$winner_photo.' alt='.$winner_name.'>' . $winner_name ." ". $winner_last_name . '</a>' . '<br>';
                    }
        }    
        else {
            $max_calls_to_api--;
            if($max_calls_to_api < 0){
                echo "С нашим сайтом что то пошло не так :( Попробуйте ещё раз, чуть позже";
                return false;
            }
            vk_like_winner_ingroup ($group_id, $post_id);
        }
    }
}           

function vk_repost_winner_ingroup ($group_id, $post_id) {
            global $max_calls_to_api, $max_winners; 
            $api_count = file_get_contents('https://api.vk.com/method/likes.getList?type=post&owner_id=-'.$group_id.'&item_id='.$post_id.'&filter=copies&friends_only=0&extended=0&offset=0&count=1&skip_own=0&access_token=813ac24f813ac24f813ac24f59815fe9228813a813ac24fda71adf311b9b9de08a5ee6f&v=5.80');
            $request = json_decode($api_count, true);           /*Запрос на количество репостов к указанному посту*/
            $count  = $request['response']['count'];       
            $used_nums =  random_array ($max_winners, $count);
        foreach ($used_nums as $winners) {    
            $api_winner_id = file_get_contents('https://api.vk.com/method/likes.getList?type=post&owner_id=-'.$group_id.'&item_id='.$post_id.'&filter=copies&friends_only=0&extended=0&offset='.$winners.'&count=1&skip_own=0&access_token=813ac24f813ac24f813ac24f59815fe9228813a813ac24fda71adf311b9b9de08a5ee6f&v=5.80');
            $request2 = json_decode($api_winner_id, true);      /*Запрос на id страницы выбранного репоста*/
            $winner_id = $request2['response']['items'][0];
            $api_group_member = file_get_contents('https://api.vk.com/method/groups.isMember?group_id='.$group_id.'&user_id='.$winner_id.'&extended=1&access_token=813ac24f813ac24f813ac24f59815fe9228813a813ac24fda71adf311b9b9de08a5ee6f&v=5.80');
            $request3 = json_decode($api_group_member, true);      /*Запрос на то состоит ли в группе человек*/
            $ismember = $request3['response']['member'];
        if ($ismember == 1) {
            $api_winner = file_get_contents ('https://api.vk.com/method/users.get?user_ids='.$winner_id.'&fields=photo_100&name_case=Nom&lang=0&access_token=813ac24f813ac24f813ac24f59815fe9228813a813ac24fda71adf311b9b9de08a5ee6f&v=5.80');
            $request4 = json_decode($api_winner, true);         /*Запрос данных победятеля по id, Имя, Фамилия, Фото*/
                    if ($request4['response'] == []) {                          /*Если предыдущий запрос пришел пустой, делаем запрос заново*/
                            $max_calls_to_api--;
                            if($max_calls_to_api < 0){
                                echo "С нашим сайтом что то пошло не так :( Попробуйте ещё раз, чуть позже";
                                return false;
                            }
                            vk_repost_winner_ingroup ($group_id, $post_id);
                    }
                    else {
                        $winner_name = $request4 ['response'][0]['first_name'];
                        $winner_last_name = $request4 ['response'][0]['last_name'];
                        $winner_photo = $request4 ['response'][0]['photo_100'];
                        echo 'Победитель:<br>' . '<a href="https://vk.com/id'.$winner_id.'" target="_blank";>' .'<img src='.$winner_photo.' alt='.$winner_name.'>' . $winner_name ." ". $winner_last_name . '</a>' . '<br>';
                    }
        }    
        else {
            $max_calls_to_api--;
            if($max_calls_to_api < 0){
                echo "С нашим сайтом что то пошло не так :( Попробуйте ещё раз, чуть позже";
                return false;
            }
            vk_repost_winner_ingroup ($group_id, $post_id);
        }
    }
}                   

?>


